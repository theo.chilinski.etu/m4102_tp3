| URI | Opération | MIME | Requête | Réponse | | :----------------------- | :---------- | :--------------------------------------------- | :-- | :---------------------------------------------------- | | /ingredients | GET | <-application/json
<-application/xml | | liste des ingrédients (I2) | | /ingredients/{id} | GET | <-application/json
<-application/xml | | un ingrédient (I2) ou 404 | | /ingredients/{id}/name | GET | <-text/plain | | le nom de l'ingrédient ou 404 | | /ingredients | POST | <-/->application/json
->application/x-www-form-urlencoded | Ingrédient (I1) | Nouvel ingrédient (I2)
409 si l'ingrédient existe déjà (même nom) | | /ingredients/{id} | DELETE | | | |