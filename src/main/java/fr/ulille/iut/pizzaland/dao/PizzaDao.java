package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;


public interface PizzaDao {
	
    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createPizzaTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (id_pizza VARCHAR(128) FOREIGN KEY REFERENCES pizzas(id), id_ingre VARCHAR(128) FOREIGN KEY REFERENCES ingredients(id))")
    void createAssociationTable();

    @Transaction
    default void createTableAndIngredientAssociation() {
      createAssociationTable();
      createPizzaTable();
    }
    
    @SqlUpdate("DROP TABLE IF EXISTS pizzas")
    void dropTable();

    @SqlUpdate("INSERT INTO pizzas (id, name) VALUES (:id, :name)")
    void insert(@BindBean Pizza pizza);

    @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
    void remove(@Bind("id") UUID id);
    
    @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
    Pizza getPizzaById(@Bind("id") UUID id);
    
    @SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
    Pizza findPizzaByName(@Bind("name") String name);
    
    @SqlQuery("SELECT * FROM pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();
}
