package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	
	private UUID id = UUID.randomUUID();
	private String name;
	private List<Ingredient> composition;

	public Pizza() {}
	
	public Pizza(String name) {
		this.name = name;
	}
	
    public Pizza(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public List<Ingredient> getPizzas() {
    	return this.composition;
    }

    public void setId(UUID id) {
        this.id = id;
    }

	public void setName(String name) {
        this.name = name;
    }
    
    public void setPizzas(List<Ingredient> compo) {
    	this.composition = compo;
    }
    
    public static PizzaDto toDto(Pizza p) {
        PizzaDto dto = new PizzaDto();
        dto.setId(p.getId());
        dto.setName(p.getName());

        return dto;
    }
    
    public Pizza fromDto(PizzaDto dto) {
    	Pizza pizza = new Pizza();
        pizza.setId(dto.getId());
        pizza.setName(dto.getName());

        return pizza;
	}
    
    public static PizzaCreateDto toCreateDto(Pizza pizza) {
        PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(pizza.getName());
            
        return dto;
      }
    	
      public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
        Pizza pizza = new Pizza();
        pizza.setName(dto.getName());

        return pizza;
      }
    
    @Override
    public String toString() {
    	StringBuilder res = new StringBuilder("Pizza [id: "+id+" name: "+name+" ingrédients: ");
    	for(Ingredient i : composition) {
    		res.append(i+", ");
    	}
    	res.substring(0, res.length()-1);
    	res.append("]");
    	return res.toString();
    }
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((composition == null) ? 0 : composition.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (composition == null) {
			if (other.composition != null)
				return false;
		} else if (!composition.equals(other.composition))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
