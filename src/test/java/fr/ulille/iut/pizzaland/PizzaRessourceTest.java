package fr.ulille.iut.pizzaland;

import static org.junit.Assert.*;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class PizzaRessourceTest extends JerseyTest {

	private static final Logger LOGGER = Logger.getLogger(PizzaRessourceTest.class.getName());
	private PizzaDao dao;
	
	@Before
    public void setEnvUp() {
      dao = BDDFactory.buildDao(PizzaDao.class);
      dao.createPizzaTable();
    }


    @After
    public void tearEnvDown() throws Exception {
        dao.dropTable();
    }
	
    @Test
    public void testGetEmptyList() {
        Response response = target("/Pizzas").request().get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        
        List<PizzaDto> Pizzas;
        Pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
        });

        assertEquals(0, Pizzas.size());

    }
    
    @Test
    public void testGetExistingPizza() {

        Pizza Pizza = new Pizza();
        Pizza.setName("Chorizo");
        dao.insert(Pizza);

        Response response = target("/Pizzas").path(Pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
        assertEquals(Pizza, result);
    }
    
    @Test
    public void testGetNotExistingPizza() {
      Response response = target("/Pizzas").path(UUID.randomUUID().toString()).request().get();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }

    @Test
    public void testCreatePizza() {
        PizzaCreateDto PizzaCreateDto = new PizzaCreateDto();
        PizzaCreateDto.setName("Chorizo");

        Response response = target("/Pizzas").request().post(Entity.json(PizzaCreateDto));

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

        assertEquals(target("/Pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
        assertEquals(returnedEntity.getName(), PizzaCreateDto.getName());
    }

    @Test
    public void testCreateSamePizza() {
        Pizza Pizza = new Pizza();
        Pizza.setName("Chorizo");
        dao.insert(Pizza);

        PizzaCreateDto PizzaCreateDto = Pizza.toCreateDto(Pizza);
        Response response = target("/Pizzas").request().post(Entity.json(PizzaCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreatePizzaWithoutName() {
        PizzaCreateDto PizzaCreateDto = new PizzaCreateDto();

        Response response = target("/Pizzas").request().post(Entity.json(PizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }
    
    @Test
    public void testDeleteExistingPizza() {
      Pizza Pizza = new Pizza();
      Pizza.setName("Chorizo");
      dao.insert(Pizza);

      Response response = target("/Pizzas/").path(Pizza.getId().toString()).request().delete();

      assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

      Pizza result = dao.findById(Pizza.getId());
      assertEquals(result, null);
   }

   @Test
   public void testDeleteNotExistingPizza() {
     Response response = target("/Pizzas").path(UUID.randomUUID().toString()).request().delete();
     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
   }
   
   @Test
   public void testGetPizzaName() {
     Pizza Pizza = new Pizza();
     Pizza.setName("Chorizo");
     dao.insert(Pizza);

     Response response = target("Pizzas").path(Pizza.getId().toString()).path("name").request().get();

     assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

     assertEquals("Chorizo", response.readEntity(String.class));
  }

  @Test
  public void testGetNotExistingPizzaName() {
    Response response = target("Pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
  }

  @Test
  public void testCreateWithForm() {
      Form form = new Form();
      form.param("name", "chorizo");

      Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
      Response response = target("Pizzas").request().post(formEntity);

      assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
      String location = response.getHeaderString("Location");
      String id = location.substring(location.lastIndexOf('/') + 1);
      Pizza result = dao.findById(UUID.fromString(id));

      assertNotNull(result);
  }

}
