package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

public class PizzaResource {

	private static final Logger LOGGER = Logger.getLogger(PizzaResource.class.getName());
    private PizzaDao pizzas;
    
    @Context
    public UriInfo uriInfo;

    public PizzaResource() {
        pizzas = BDDFactory.buildDao(PizzaDao.class);
        pizzas.createPizzaTable();
    }

    @GET
    public List<PizzaDto> getAll() {
        LOGGER.info("PizzaResource:getAll");

        List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
        LOGGER.info(l.toString());
        return l;
    }
    
    @GET
    @Path("{id}")
    @Produces({ "application/json", "application/xml" })
    public PizzaDto getOnePizza(@PathParam("id") UUID id) {
    	 LOGGER.info("getOnePizza(" + id + ")");
         try {
             Pizza Pizza = pizzas.findById(id);
             LOGGER.info(Pizza.toString());
             return Pizza.toDto(Pizza);
         } catch (Exception e) {
             throw new WebApplicationException(Response.Status.NOT_FOUND);
         }
    }
    
    @POST
    @Consumes("application/json")
    public Response createPizza(PizzaCreateDto PizzaCreateDto) {
        Pizza existing = Pizzas.findPizzaByName(PizzaCreateDto.getName());
        if (existing != null) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        try {
            Pizza Pizza = Pizza.fromPizzaCreateDto(PizzaCreateDto);
            pizzas.insert(Pizza);
            PizzaDto PizzaDto = Pizza.toDto(Pizza);

            URI uri = uriInfo.getAbsolutePathBuilder().path(Pizza.getId().toString()).build();

            return Response.created(uri).entity(PizzaDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }

    }

    @DELETE
    @Path("{id}")
    public Response deletePizza(@PathParam("id") UUID id) {
      if ( pizzas.findById(id) == null ) {
        throw new WebApplicationException(Response.Status.NOT_FOUND);
      }

      pizzas.remove(id);

      return Response.status(Response.Status.ACCEPTED).build();
    }
    
    @GET
    @Path("{id}/name")
    public String getPizzaName(@PathParam("id") UUID id) {
        Pizza Pizza = pizzas.findById(id);

        if (Pizza == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        return Pizza.getName();
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response createPizza(@FormParam("name") String name) {
      Pizza existing = pizzas.findPizzaByName(name);
      if (existing != null) {
        throw new WebApplicationException(Response.Status.CONFLICT);
      }

      try {
        Pizza Pizza = new Pizza();
        Pizza.setName(name);

        pizzas.insert(Pizza);

        Pizza existing = pizzas.findPizzaByName(name);
        if (existing != null) {
          throw new WebApplicationException(Response.Status.CONFLICT);
        }

        try {
          Pizza Pizza = new Pizza();
          Pizza.setName(name);

          pizzas.insert(Pizza);

          PizzaDto PizzaDto = Pizza.toDto(Pizza);

          URI uri = uriInfo.getAbsolutePathBuilder().path("" + Pizza.getId()).build();

          return Response.created(uri).entity(PizzaDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
      
        PizzaDto PizzaDto = Pizza.toDto(Pizza);

        URI uri = uriInfo.getAbsolutePathBuilder().path("" + Pizza.getId()).build();

        return Response.created(uri).entity(PizzaDto).build();
      } catch (Exception e) {
          e.printStackTrace();
          throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
      }
    }
	
}
